# zusi
Source files of the OeBB 1044/1144 models created for Zusi 3 (http://www.zusi.de), published under the Creative Commons license (CC-BY-SA, summary: http://creativecommons.org/licenses/by-sa/3.0/, full license: https://creativecommons.org/licenses/by-sa/3.0/legalcode).
